----------------------------------------------------------------------------------
-- Company: NTNU - Design of Digital Systems 1
-- Engineers: Johannes Carl Fahr
--            Martin Schau Draugsvoll, 
--            Tanvir Hassan Tusher
-- 
-- Create Date: 25.10.2022 23:52:01
-- Module Name: mod_mult_tb - Testbench
-- Project Name: Exponentiation
-- Description: Testbench for mod_mult
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity mod_mult_tb is
	generic (
		C_block_size : integer := 256
	);
end mod_mult_tb;

architecture test_mod_mult of mod_mult_tb is

    type t_mod_in_if is record
        a           : STD_LOGIC_VECTOR( C_block_size-1 downto 0 ); -- to dut
        b           : STD_LOGIC_VECTOR( C_block_size-1 downto 0 ); -- to dut
        modulus     : STD_LOGIC_VECTOR( C_block_size-1 downto 0 ); -- to dut
        valid_in    : STD_LOGIC;                                   -- to dut
        ready_out   : STD_LOGIC;                                   -- to dut
    end record;
    
    type t_mod_out_if is record
        result      : STD_LOGIC_VECTOR( C_block_size-1 downto 0 ); -- from dut
        result_ref  : STD_LOGIC_VECTOR( C_block_size-1 downto 0 ); -- from GOLDEN
        valid_out   : STD_LOGIC;                                   -- from dut
        ready_in    : STD_LOGIC;                                   -- from dut
    end record;

    -- Constants
    constant CLK_PERIOD : time := 2 ns;
    constant RESET_TIME : time := 2 ns;

    signal mult_if      : t_mod_in_if;
    signal mult_out_if  : t_mod_out_if;
	signal clk_tb	    : STD_LOGIC := '0';
	signal reset_n 		: STD_LOGIC := '0';
	
    
    procedure check(
        constant a_in       : in STD_LOGIC_VECTOR( C_block_size-1 downto 0 );
        constant b_in       : in STD_LOGIC_VECTOR( C_block_size-1 downto 0 );
        constant modulus_in : in STD_LOGIC_VECTOR( C_block_size-1 downto 0 );
        --signal result_out   : inout std_logic_vector;
        --signal result_ref   : in std_logic_vector;
        signal mod_op_if    : out t_mod_in_if;
        signal mod_out_if   : in t_mod_out_if;
        constant message    : in string) is 
    begin
    
        wait for CLK_PERIOD;
        
        assert(mod_out_if.ready_in = '1')
            report "Input not ready: " & message
            severity Failure;
            
        if (mod_out_if.ready_in = '1') then
            mod_op_if.valid_in    <= '1';
            mod_op_if.a           <= a_in;
            mod_op_if.b           <= b_in;
            mod_op_if.modulus     <= modulus_in;
            mod_op_if.ready_out   <= '0';
            
            wait for CLK_PERIOD;
            
            mod_op_if.valid_in    <= '0';
            mod_op_if.a           <= (others => 'U');
            mod_op_if.b           <= (others => 'U');
            mod_op_if.modulus     <= (others => 'U');
            mod_op_if.ready_out   <= '1';
            
            -- Wait for results
            wait until rising_edge(mod_out_if.valid_out);
            
            assert(mod_out_if.result = mod_out_if.result_ref)
                report "Wrong result: " & message
                severity Failure;
        end if;
        
        wait for CLK_PERIOD;
        mod_op_if.valid_in    <= '0';
        
    end;
    
    procedure check_uint(
        constant a_in       : in natural;
        constant b_in       : in natural;
        constant modulus_in : in natural;
        signal mod_op_if    : out t_mod_in_if;
        signal mod_out_if   : in t_mod_out_if;
        constant message    : in string) is 
    begin
        check(  STD_LOGIC_VECTOR(to_unsigned(a_in, mod_op_if.a'length)),
                STD_LOGIC_VECTOR(to_unsigned(b_in, mod_op_if.b'length)),
                STD_LOGIC_VECTOR(to_unsigned(modulus_in, mod_op_if.modulus'length)),
                mod_op_if, mod_out_if, message);
    end;
    
    procedure reset(signal reset_n : out std_logic) is 
    begin
        reset_n <= '0';
        wait for RESET_TIME;
        reset_n <= '1';
    end;

begin

    DUT : entity work.mod_mult
    port map (
        a         => mult_if.a        ,
        b         => mult_if.b        ,
        modulus   => mult_if.modulus  ,
        valid_in  => mult_if.valid_in ,
        ready_in  => mult_out_if.ready_in ,
        ready_out => mult_if.ready_out,
        valid_out => mult_out_if.valid_out,
        result    => mult_out_if.result   ,
        clk       => clk_tb      ,
        reset_n   => reset_n
    );
		
	GOLDEN : entity work.mod_mult_ref
    port map (
        a         => mult_if.a            ,
        b         => mult_if.b            ,
        modulus   => mult_if.modulus      ,
        valid_in  => mult_if.valid_in     ,
        ready_out => mult_if.ready_out    ,
        result    => mult_out_if.result_ref   ,
        clk       => clk_tb          ,
        reset_n   => reset_n
    );
    
    -- Clock generation
    clk_tb <= not clk_tb after CLK_PERIOD/2;
    

    -- Stimuli generation
    stimuli_proc: process begin
        reset(reset_n);
        
        wait for 5*CLK_PERIOD;
        
        assert true;
            report "********************************************************************************";
            report "STARTING TESTS";
            report "********************************************************************************";
        
        -- Check first test vector
        check_uint(12, 11, 10, mult_if, mult_out_if, "1");
        
        -- Check if output data is kept when receiver not ready
        mult_if.valid_in    <= '1';
        mult_if.a           <= std_logic_vector(to_unsigned(14, mult_if.a'length));
        mult_if.b           <= std_logic_vector(to_unsigned(123, mult_if.b'length));
        mult_if.modulus     <= std_logic_vector(to_unsigned(233, mult_if.modulus'length));
        mult_if.ready_out   <= '0';
            
        wait for CLK_PERIOD;
        
        mult_if.valid_in    <= '0';
        mult_if.a           <= (others => 'U');
        mult_if.b           <= (others => 'U');
        mult_if.modulus     <= (others => 'U');
        mult_if.ready_out   <= '0';
        
        -- Wait for results
        wait until rising_edge(mult_out_if.valid_out);
        
        wait for 100*CLK_PERIOD;
        
        assert(mult_out_if.result = mult_out_if.result_ref)
            report "Wrong result after delayed readyness"
            severity error;
            
        mult_if.ready_out   <= '1';
        wait for CLK_PERIOD;
        
        -- Check several values
        check_uint(123, 456, 789, mult_if, mult_out_if, "2");
        check_uint(35454, 3466553, 234354, mult_if, mult_out_if, "3");
            
        check(  x"78f9baf32e8505cbc9a28fed4d5791dce46508c3d1636232bf91f5d0b6632a9f",
                x"85ee722363960779206a2b37cc8b64b5fc12a934473fa0204bbaaf714bc90c01", 
                x"71ca80617856c964e2f92dde82841cc7ce81210f56bf18a32d2ee2ada64586d6",
                mult_if, mult_out_if, "4");
        check(  x"08f9baf32e8505cbc9a28fed4d5791dce46508c3d1636232bf91f5d0b6632a9f",
                x"78f9baf32e8505cbc9a28fed4d5791dce46508c3d1636232bf91f5d0b6632a9f", 
                x"81ca80617856c964e2f92dde82841cc7ce81210f56bf18a32d2ee2ada64586d6",
                mult_if, mult_out_if, "5");
                
        assert true;
            report "********************************************************************************";
            report "ALL TESTS FINISHED SUCCESSFULLY";
            report "********************************************************************************";
            report "ENDING SIMULATION..." severity Failure;
        
        wait;
        
    end process;
    
end test_mod_mult;
