library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity exponentiation_tb is
	generic (
		C_block_size : integer := 256
	);
end exponentiation_tb;


architecture test_exponentiation of exponentiation_tb is

    type t_exp_in_if is record
        message     : STD_LOGIC_VECTOR( C_block_size-1 downto 0 ); -- to dut
        key         : STD_LOGIC_VECTOR( C_block_size-1 downto 0 ); -- to dut
        modulus     : STD_LOGIC_VECTOR( C_block_size-1 downto 0 ); -- to dut
        valid_in    : STD_LOGIC;                                   -- to dut
        ready_out   : STD_LOGIC;                                   -- to dut
    end record;
    
    type t_exp_out_if is record
        result      : STD_LOGIC_VECTOR( C_block_size-1 downto 0 ); -- from dut
        valid_out   : STD_LOGIC;                                   -- from dut
        ready_in    : STD_LOGIC;                                   -- from dut
    end record;
    
    -- Constants
    constant CLK_PERIOD : time := 2 ns;
    constant RESET_TIME : time := 2 ns;

    signal exp_if       : t_exp_in_if;
    signal exp_out_if   : t_exp_out_if;
	signal clk_tb	    : STD_LOGIC := '0';
	signal reset_n 		: STD_LOGIC := '0';
	
	
	procedure check(
        constant message_in : in STD_LOGIC_VECTOR( C_block_size-1 downto 0 );
        constant key_in     : in STD_LOGIC_VECTOR( C_block_size-1 downto 0 );
        constant modulus_in : in STD_LOGIC_VECTOR( C_block_size-1 downto 0 );
        --signal result_out   : inout std_logic_vector;
        constant result_ref : in std_logic_vector;
        signal exp_op_if    : out t_exp_in_if;
        signal exp_out_if   : in t_exp_out_if;
        constant message    : in string) is 
    begin
        wait for 2*CLK_PERIOD;
        
        assert(exp_out_if.ready_in = '1')
            report "Input not ready: " & message
            severity Failure;
            
        if (exp_out_if.ready_in = '1') then
            exp_op_if.valid_in    <= '1';
            exp_op_if.message     <= message_in;
            exp_op_if.key         <= key_in;
            exp_op_if.modulus     <= modulus_in;
            exp_op_if.ready_out   <= '0';
            
            wait for CLK_PERIOD;
            
            exp_op_if.valid_in    <= '0';
            exp_op_if.message     <= (others => 'U');
            exp_op_if.key         <= (others => 'U');
            exp_op_if.modulus     <= (others => 'U');
            exp_op_if.ready_out   <= '1';
            
            -- Wait for results
            wait until rising_edge(exp_out_if.valid_out);
            
            assert(exp_out_if.result = result_ref)
                report "Wrong result: " & message
                severity Failure;
        end if;
        
        wait for CLK_PERIOD;
        exp_op_if.valid_in    <= '0';    
    end;
    
    procedure check_uint(
        constant message_in : in natural;
        constant key_in     : in natural;
        constant modulus_in : in natural;
        constant result_ref : in natural;
        signal exp_op_if    : out t_exp_in_if;
        signal exp_out_if   : in t_exp_out_if;
        constant message    : in string) is 
    begin
        check(  STD_LOGIC_VECTOR(to_unsigned(message_in, exp_op_if.message'length)),
                STD_LOGIC_VECTOR(to_unsigned(key_in, exp_op_if.key'length)),
                STD_LOGIC_VECTOR(to_unsigned(modulus_in, exp_op_if.modulus'length)),
                STD_LOGIC_VECTOR(to_unsigned(result_ref, exp_out_if.result'length)),
                exp_op_if, exp_out_if, message);
    end;
    
    procedure reset(signal reset_n : out std_logic) is 
    begin
        reset_n <= '0';
        wait for RESET_TIME;
        reset_n <= '1';
    end;

begin
	DUT : entity work.exponentiation
		port map (
			message   => exp_if.message      ,
			key       => exp_if.key          ,
			valid_in  => exp_if.valid_in     ,
			ready_in  => exp_out_if.ready_in ,
			ready_out => exp_if.ready_out    ,
			valid_out => exp_out_if.valid_out,
			result    => exp_out_if.result   ,
			modulus   => exp_if.modulus      ,
			clk       => clk_tb              ,
			reset_n   => reset_n
		);
		
    -- Clock generation
    clk_tb <= not clk_tb after CLK_PERIOD/2;

    -- Stimuli generation
    stimuli_proc: process begin
        reset(reset_n);
        
        wait for 5*CLK_PERIOD;
        
        assert true;
            report "********************************************************************************";
            report "STARTING TESTS";
            report "********************************************************************************";
        
        
        -- Check first test vector
        check_uint(2, 1, 2, 0, exp_if, exp_out_if, "0");
        reset(reset_n);
        check_uint(12, 11, 10, 8, exp_if, exp_out_if, "1");
        check_uint(2, 1, 2, 0, exp_if, exp_out_if, "2");
        check_uint(2, 2, 2, 0, exp_if, exp_out_if, "3");
        check_uint(3, 5, 7, 5, exp_if, exp_out_if, "4");
        check_uint(3, 5, 7, 5, exp_if, exp_out_if, "5");
    
    
    
        assert true;
            report "********************************************************************************";
            report "ALL TESTS FINISHED SUCCESSFULLY";
            report "********************************************************************************";
            report "ENDING SIMULATION..." severity Failure;
        
        wait;
        
    end process;
    
end test_exponentiation;
