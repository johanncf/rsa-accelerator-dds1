----------------------------------------------------------------------------------
-- Company: NTNU - Design of Digital Systems 1
-- Engineers: Johannes Carl Fahr
--            Martin Schau Draugsvoll, 
--            Tanvir Hassan Tusher
-- 
-- Create Date: 25.10.2022 18:56:53
-- Module Name: mod_mult - Behavioral
-- Project Name: Exponentiation
-- Description: This entity performs a modular multiplicatio
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity mod_mult is
	generic (
		C_block_size : integer := 256
	);
    Port ( 
        -- Clocks and resets
        clk             : in STD_LOGIC;
        reset_n         : in STD_LOGIC;
        
        -- Input signals
        a               : in STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
        b               : in STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
        modulus         : in STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
        valid_in        : in STD_LOGIC;
        ready_out       : in STD_LOGIC;
        
        -- Output signals
        result          : out STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
        valid_out       : out STD_LOGIC;
        ready_in        : out STD_LOGIC
    );
end mod_mult;


architecture rtl of mod_mult is
    -- Datapath signals
    signal a_next               : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal a_r                  : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal b_r                  : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal modulus_r            : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal b_reg_shift          : STD_LOGIC;
    signal load_in_reg          : STD_LOGIC;
    signal p_r                  : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal p_sum                : STD_LOGIC_VECTOR ( C_block_size+1 downto 0 );
    signal p_sum_r              : STD_LOGIC_VECTOR ( C_block_size+1 downto 0 );
    signal p_prime              : STD_LOGIC_VECTOR ( C_block_size+1 downto 0 );
    signal modulus_next         : STD_LOGIC_VECTOR ( C_block_size   downto 0 );
    signal p_sum_reg_en         : STD_LOGIC;
    signal p_reg_en             : STD_LOGIC;
    
    -- Controller signals
    type state is (IDLE, INIT, WORK, WORK_SHIFT, DONE);
    signal curr_state, next_state : state;
    signal valid_out_i          : STD_LOGIC;
    signal ready_in_i           : STD_LOGIC;
    signal reset_n_i            : STD_LOGIC;
    signal b_shift_counter_next : UNSIGNED(8 downto 0);
    signal b_shift_counter_r    : UNSIGNED(8 downto 0);
begin
    -- Shift register for b input
    -- The LSB is shifted out for bitwise multiplication
    process (clk, reset_n) begin
        if (reset_n = '0') then
            b_r <= (others => '0');
        elsif (clk'event and clk='1') then
            if (load_in_reg = '1') then
                b_r <= b;
            elsif b_reg_shift = '1' then
                b_r <=  b_r(b_r'high-1 downto 0) & '0';
            else
                b_r <= b_r;
            end if;
        end if;
    end process;
    
    
    -- Buffer register for inputs
    process (clk, reset_n) begin
        if (reset_n = '0') then
            a_r         <= (others => '0');
            modulus_r   <= (others => '0');
        elsif (clk'event and clk='1') then
            if (load_in_reg = '1') then
                a_r <= a;
                modulus_r <= modulus;
            else 
                a_r <= a_r;
                modulus_r <= modulus_r;
            end if;
        end if;
    end process;
    
    
    -- Computation of modulus_next
    process (p_sum_r, modulus_r) begin
        if(p_sum_r >= ('0' & modulus_r & '0')) then
            modulus_next <= modulus_r & '0';
        elsif(p_sum_r >= ("00" & modulus_r)) then
            modulus_next <= '0' & modulus_r;
        else
            modulus_next <= (others => '0');
        end if;
    end process;
    
    
    -- Bitwise multiplication of a and b
    process (a_r, b_r(b_r'high)) begin
        if (b_r(b_r'high) = '1') then
            a_next <= a_r;
        else
            a_next <= (others => '0');
        end if;
    end process;
        
    p_sum <= std_logic_vector(unsigned("00" & a_next) + unsigned('0' & p_r & '0'));
    
    -- Buffer register for p_sum
    process (clk, reset_n, reset_n_i) begin
        if (reset_n = '0' or reset_n_i = '0') then
            p_sum_r <= (others => '0');
        elsif (clk'event and clk='1') then
            if p_sum_reg_en = '1' then
                p_sum_r <= p_sum;
            else
                p_sum_r <= p_sum_r;
            end if;
        end if;
    end process;
    
    
    p_prime <= std_logic_vector(unsigned(p_sum_r) - unsigned('0' & modulus_next));
    
    
    -- Buffer register for p
    process (clk, reset_n, reset_n_i) begin
        if(reset_n = '0' or reset_n_i = '0') then
            p_r <= (others => '0');
        elsif(clk'event and clk='1') then
            if(p_reg_en ='1') then
                p_r <= p_prime( C_block_size-1 downto 0 );
            else
                p_r <= p_r;
            end if;
        end if;
    end process;
    
    
    -- CONTROLLER STATE MACHINE
    CombProc : process (valid_in, ready_out, curr_state, b_shift_counter_r) begin
        case (curr_state) is
        when IDLE =>
            reset_n_i   <= '1';
            ready_in_i  <= '1';
            p_reg_en    <= '0';
            p_sum_reg_en<= '0';
            b_reg_shift <= '0';
            b_shift_counter_next <= (others => '0');
            valid_out_i <= '0';
            if valid_in = '1' then
                next_state <= INIT;
            else
                next_state <= IDLE;
            end if;
        when INIT =>
            reset_n_i   <= '0';
            ready_in_i  <= '0';
            p_reg_en    <= '0';
            p_sum_reg_en<= '0';
            b_reg_shift <= '0';
            b_shift_counter_next <= (others => '0');
            valid_out_i <= '0';
            next_state <= WORK;
        when WORK =>
            reset_n_i   <= '1';
            ready_in_i  <= '0';
            p_reg_en    <= '0';
            p_sum_reg_en<= '1';
            b_reg_shift <= '1';
            b_shift_counter_next <= b_shift_counter_r + 1;
            valid_out_i <= '0';
            next_state <= WORK_SHIFT;
        when WORK_SHIFT =>
            reset_n_i   <= '1';
            ready_in_i  <= '0';
            p_reg_en    <= '1';
            p_sum_reg_en<= '0';
            b_reg_shift <= '0';
            b_shift_counter_next <= b_shift_counter_r;
            valid_out_i <= '0';
            if b_shift_counter_r = C_block_size then
                next_state <= DONE;
            else
                next_state <= WORK;
            end if;
        when DONE =>
            reset_n_i   <= '1';
            ready_in_i  <= '0';
            p_reg_en    <= '0';
            p_sum_reg_en<= '0';
            b_reg_shift <= '0';
            b_shift_counter_next <= b_shift_counter_r;
            valid_out_i <= '1';
            if ready_out = '1' then
                next_state <= IDLE;
            else
                next_state <= DONE;
            end if;
        when others =>
            reset_n_i   <= '1';
            ready_in_i  <= '0';
            p_reg_en    <= '0';
            p_sum_reg_en<= '0';
            b_reg_shift <= '0';
            b_shift_counter_next <= b_shift_counter_r;
            valid_out_i <= '0';
            next_state  <= IDLE;
        end case;
    end process CombProc;
    
    SyncProc : process (reset_n, clk) begin
        if (reset_n = '0') then
            curr_state <= IDLE;
            b_shift_counter_r <= (others => '0');
        elsif (clk'event and clk='1') then
            b_shift_counter_r <= b_shift_counter_next;
            curr_state <= next_state;
        end if;
    end process SyncProc;
    
    
    load_in_reg <= valid_in and ready_in_i;
    valid_out   <= valid_out_i;
    ready_in    <= ready_in_i;
    result      <= p_r;             -- route p_r to output

end rtl;
