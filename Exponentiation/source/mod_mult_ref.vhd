----------------------------------------------------------------------------------
-- Company: NTNU - Design of Digital Systems 1
-- Engineers: Johannes Carl Fahr
--            Martin Schau Draugsvoll, 
--            Tanvir Hassan Tusher
-- 
-- Create Date: 25.10.2022 18:56:53
-- Design Name: 
-- Module Name: mod_mult - Behavioral
-- Project Name: exponentiation
-- Target Devices: 
-- Tool Versions: 
-- Description: This entity performs a modular multiplicatio
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity mod_mult_ref is
	generic (
		C_block_size : integer := 256
	);
    Port ( 
        -- Clocks and resets
        clk             : in std_logic;
        reset_n         : in std_logic;
        
        -- Input signals
        a               : in STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
        b               : in STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
        modulus         : in STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
        valid_in        : in STD_LOGIC;
        ready_out       : in STD_LOGIC;
        
        -- Output signals
        result          : out STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
        valid_out       : out STD_LOGIC;
        ready_in        : out STD_LOGIC
    );
end mod_mult_ref;


architecture behavior of mod_mult_ref is
    signal result_r     : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
begin

    process (clk, reset_n) begin
        if(reset_n = '0') then
            result_r <= (others => '0');
        elsif(clk'event and clk='1') then
            if (valid_in = '1') then
                result_r    <= std_logic_vector((unsigned(a)*unsigned(b)) mod unsigned(modulus));
                valid_out   <= '1';
                ready_in    <= '1';
            else
                result_r    <= result_r;
            end if;
        end if;
    end process;

    result <= result_r;
end behavior;