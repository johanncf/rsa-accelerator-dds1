----------------------------------------------------------------------------------
-- Company: NTNU - Design of Digital Systems 1
-- Engineers: Johannes Carl Fahr
--            Martin Schau Draugsvoll, 
--            Tanvir Hassan Tusher
-- 
-- Create Date: 16.11.2022 18:00:32
-- Module Name: exponentiation - Behavioral
-- Project Name: Exponentiation
-- Description: This entity performs a modular exponentiation:
-- 
--              result = message ** key_e mod modulus.
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity exponentiation is
	generic (
		C_block_size : integer := 256
	);
	port (
		--input controll
		valid_in	: in STD_LOGIC;
		ready_in	: out STD_LOGIC;

		--input data
		message 	: in STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
		key 		: in STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );

		--ouput controll
		ready_out	: in STD_LOGIC;
		valid_out	: out STD_LOGIC;

		--output data
		result 		: out STD_LOGIC_VECTOR(C_block_size-1 downto 0);

		--modulus
		modulus 	: in STD_LOGIC_VECTOR(C_block_size-1 downto 0);

		--utility
		clk 		: in STD_LOGIC;
		reset_n 	: in STD_LOGIC
	);
end exponentiation;


architecture rtl_xor of exponentiation is
begin
	result <= message xor modulus;
	ready_in <= ready_out;
	valid_out <= valid_in;
end rtl_xor;


architecture rtl of exponentiation is
    -- Datapath signals
    --signal message_r            : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal key_r                : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal modulus_r            : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal c                    : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal c_r                  : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal m_r                  : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal c_intermediate       : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal m_intermediate       : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal c_next               : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal m_next               : STD_LOGIC_VECTOR ( C_block_size-1 downto 0 );
    signal reg_en               : STD_LOGIC;
    signal reg_shift		    : STD_LOGIC;
    signal load_in_reg          : STD_LOGIC;

    -- Controller signals
    type state is (IDLE, INIT, START_MOD_MULT, WAIT_FOR_MOD_MULT, RESTART_MOD_MULT, DONE, CLEANUP);
    signal curr_state, next_state : state;
    signal valid_out_i          : STD_LOGIC;
    signal ready_in_i           : STD_LOGIC;
    signal key_shift_counter_next: UNSIGNED(8 downto 0);
    signal key_shift_counter_r  : UNSIGNED(8 downto 0);
    signal mult_valid_in        : STD_LOGIC;
    signal mult_ready_out       : STD_LOGIC;
    signal c_ready_in           : STD_LOGIC;
    signal m_ready_in           : STD_LOGIC;
    signal c_valid_out  	    : STD_LOGIC;
    signal m_valid_out  	    : STD_LOGIC;
    signal mult_valid_out		: STD_LOGIC;
    signal mult_ready_in        : STD_LOGIC;
begin

    --Modular multiplication for the c block
    c_mod_mult : entity work.mod_mult
    port map (
        a         => c_r           ,
        b         => m_r           ,
        modulus   => modulus_r     ,
        valid_in  => mult_valid_in ,
        ready_in  => c_ready_in    ,
        ready_out => mult_ready_out,
        valid_out => c_valid_out   ,
        result    => c_next        ,
        clk       => clk           ,
        reset_n   => reset_n
    );

    --Modular multiplication for the m block
    m_mod_mult : entity work.mod_mult
    port map (
        a         => m_r           ,
        b         => m_r           ,
        modulus   => modulus_r     ,
        valid_in  => mult_valid_in ,
        ready_in  => m_ready_in    ,
        ready_out => mult_ready_out,
        valid_out => m_valid_out   ,
        result    => m_next        ,
        clk       => clk           ,
        reset_n   => reset_n
    );
    
    mult_valid_out <= c_valid_out and m_valid_out;
    mult_ready_in  <= c_ready_in and m_ready_in;


    -- Shift register for key input
    -- The LSB is shifted out for bitwise multiplication
    process (clk, reset_n) begin
        if (reset_n = '0') then
            key_r <= (others => '0');
        elsif (clk'event and clk='1') then
            if (load_in_reg = '1') then
                key_r <= key;
            elsif reg_shift = '1' then
                key_r <= '0' & key_r(key_r'high downto 1);
            else
                key_r <= key_r;
            end if;
        end if;
    end process;


    -- Buffer register for inputs
    process (clk, reset_n) begin
        if (reset_n = '0') then
            --message_r   <= (others => '0');
            modulus_r   <= (others => '0');
        elsif (clk'event and clk='1') then
            if (load_in_reg = '1') then
                --message_r <= message;
                modulus_r <= modulus;
            else 
                --message_r <= message_r;
                modulus_r <= modulus_r;
            end if;
        end if;
    end process;
    
    
    -- Computation of c_intermediate and m_intermediate
    process (c, message, m_next, load_in_reg) begin --message_r
        if (load_in_reg = '1') then
            c_intermediate <= (0 => '1', others => '0');
            m_intermediate <= message;
        else
            c_intermediate <= c; 
            m_intermediate <= m_next;
        end if;
    end process;


    --Buffer register for c and m
    process (clk, reset_n) begin
        if(reset_n = '0') then
            c_r <= (others => '0');
            m_r <= (others => '0');
        elsif(clk'event and clk='1') then
            if(reg_en ='1') then
                c_r <= c_intermediate;
                m_r <= m_intermediate;
            else
                c_r <= c_r;
                m_r <= m_r;
            end if;
        end if;
    end process;


    -- Computation of c
    process (c_next, c_r, key_r(0)) begin
        if (key_r(0) = '1') then
            c <= c_next;
        else
            c <= c_r;
        end if;
    end process;
    

    -- CONTROLLER STATE MACHINE
    CombProc : process (valid_in, ready_out, curr_state, key_shift_counter_r, mult_ready_in, mult_valid_out) begin
        case (curr_state) is
        when IDLE =>
            ready_in_i      <= '1';
            reg_shift       <= '0';
            mult_valid_in   <= '0';
            mult_ready_out  <= '0';
            key_shift_counter_next <= (others => '0');
            valid_out_i     <= '0';
            if (valid_in = '1') then
                next_state  <= START_MOD_MULT;
            else
                next_state  <= IDLE;
            end if;
        when START_MOD_MULT =>
            ready_in_i      <= '0';
            reg_shift       <= '0';
            mult_valid_in   <= '1';
            mult_ready_out  <= '1';
            key_shift_counter_next <= key_shift_counter_r + 1;
            valid_out_i     <= '0';
            if (mult_ready_in = '1') then
                next_state  <= WAIT_FOR_MOD_MULT;
            else 
                next_state  <= START_MOD_MULT;
            end if;
        when WAIT_FOR_MOD_MULT =>
            ready_in_i      <= '0';
            reg_shift       <= '0';
            mult_valid_in   <= '0';
            mult_ready_out  <= '1';
            key_shift_counter_next <= key_shift_counter_r;
            valid_out_i     <= '0';
            if (mult_valid_out = '1' and key_shift_counter_r = C_block_size) then
                next_state      <= DONE;
            elsif (mult_valid_out = '1') then
                next_state      <= RESTART_MOD_MULT;
            else
                next_state      <= WAIT_FOR_MOD_MULT;
            end if;
        when RESTART_MOD_MULT =>
            ready_in_i      <= '0';
            reg_shift       <= '1';
            mult_valid_in   <= '1';
            mult_ready_out  <= '1';
            key_shift_counter_next <= key_shift_counter_r + 1;
            valid_out_i     <= '0';
            next_state      <= WAIT_FOR_MOD_MULT;
        when DONE =>
            ready_in_i      <= '0';
            reg_shift       <= '0';
            mult_valid_in   <= '0';
            mult_ready_out  <= '1';
            key_shift_counter_next <= key_shift_counter_r;
            valid_out_i     <= '1';
            if ready_out = '1' then
                next_state  <= IDLE;
            else
                next_state  <= DONE;
            end if;
        when CLEANUP =>
            ready_in_i      <= '0';
            reg_shift       <= '0';
            mult_valid_in   <= '0';
            mult_ready_out  <= '0';
            key_shift_counter_next <= key_shift_counter_r;
            valid_out_i     <= '1';
            next_state  <= IDLE;
        when others =>
            ready_in_i      <= '0';
            reg_shift       <= '0';
            mult_valid_in   <= '0';
            mult_ready_out  <= '0';
            key_shift_counter_next <= (others => '0');
            valid_out_i     <= '0';
            next_state      <= IDLE;
        end case;
    end process CombProc;

    SyncProc : process (clk, reset_n) begin
        if (reset_n = '0') then
            curr_state <= IDLE;
            key_shift_counter_r <= (others => '0');
        elsif (clk'event and clk='1') then
            key_shift_counter_r <= key_shift_counter_next;
            curr_state <= next_state;
        end if;
    end process SyncProc;

    load_in_reg <= valid_in and ready_in_i;
    reg_en      <= load_in_reg or mult_valid_out;
    valid_out   <= valid_out_i;
    ready_in    <= ready_in_i;
    result      <= c_r;             -- route c_r to output

end rtl;
