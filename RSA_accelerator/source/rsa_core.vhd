--------------------------------------------------------------------------------
-- Author       : Oystein Gjermundnes
-- Organization : Norwegian University of Science and Technology (NTNU)
--                Department of Electronic Systems
--                https://www.ntnu.edu/ies
-- Course       : TFE4141 Design of digital systems 1 (DDS1)
-- Year         : 2018-2019
-- Project      : RSA accelerator
-- License      : This is free and unencumbered software released into the
--                public domain (UNLICENSE)
--------------------------------------------------------------------------------
-- Purpose:
--   RSA encryption core template. This core currently computes
--   C = M xor key_n
--
--   Replace/change this module so that it implements the function
--   C = M**key_e mod key_n.
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity rsa_core is
	generic (
		-- Users to add parameters here
		C_BLOCK_SIZE          : integer := 256;
		C_NUMBER_OF_CORES     : integer := 8
	);
	port (
		-----------------------------------------------------------------------------
		-- Clocks and reset
		-----------------------------------------------------------------------------
		clk                    :  in std_logic;
		reset_n                :  in std_logic;

		-----------------------------------------------------------------------------
		-- Slave msgin interface
		-----------------------------------------------------------------------------
		-- Message that will be sent out is valid
		msgin_valid             : in std_logic;
		-- Slave ready to accept a new message
		msgin_ready             : out std_logic;
		-- Message that will be sent out of the rsa_msgin module
		msgin_data              :  in std_logic_vector(C_BLOCK_SIZE-1 downto 0);
		-- Indicates boundary of last packet
		msgin_last              :  in std_logic;

		-----------------------------------------------------------------------------
		-- Master msgout interface
		-----------------------------------------------------------------------------
		-- Message that will be sent out is valid
		msgout_valid            : out std_logic;
		-- Slave ready to accept a new message
		msgout_ready            :  in std_logic;
		-- Message that will be sent out of the rsa_msgin module
		msgout_data             : out std_logic_vector(C_BLOCK_SIZE-1 downto 0);
		-- Indicates boundary of last packet
		msgout_last             : out std_logic;

		-----------------------------------------------------------------------------
		-- Interface to the register block
		-----------------------------------------------------------------------------
		key_e_d                 :  in std_logic_vector(C_BLOCK_SIZE-1 downto 0);
		key_n                   :  in std_logic_vector(C_BLOCK_SIZE-1 downto 0);
		rsa_status              : out std_logic_vector(31 downto 0)
	);
end rsa_core;

architecture rtl of rsa_core is
    type DATA_ARRAY is array (0 to C_NUMBER_OF_CORES-1) of std_logic_vector(C_BLOCK_SIZE-1 downto 0);
    signal data_out             : DATA_ARRAY;
    signal valid_in             : std_logic_vector(C_NUMBER_OF_CORES-1 downto 0);
    signal valid_out            : std_logic_vector(C_NUMBER_OF_CORES-1 downto 0);
    signal ready_in             : std_logic_vector(C_NUMBER_OF_CORES-1 downto 0);
    signal ready_out            : std_logic_vector(C_NUMBER_OF_CORES-1 downto 0);
    
    -- Controller signals
    type CORE_STATE is (IDLE, ENCODE);
    signal curr_state, next_state : CORE_STATE;
    signal core_in_use          : std_logic_vector(C_NUMBER_OF_CORES-1 downto 0);
    signal core_in_use_r        : std_logic_vector(C_NUMBER_OF_CORES-1 downto 0);
    signal core_processes_last  : std_logic_vector(C_NUMBER_OF_CORES-1 downto 0);
    signal core_processes_last_r: std_logic_vector(C_NUMBER_OF_CORES-1 downto 0);
    signal core_number_in_next  : integer;
    signal core_number_out_next : integer;
    signal core_number_in_r     : integer;
    signal core_number_out_r    : integer;
begin

    -- Generate C_NUMBER_OF_CORES exponentiation cores
    exp_cores: for i in 0 to C_NUMBER_OF_CORES-1 generate
        exp_core : entity work.exponentiation
        generic map (
            C_block_size => C_BLOCK_SIZE
        )
        port map (
            message   => msgin_data  ,
            key       => key_e_d     ,
            valid_in  => valid_in(i) ,
            ready_in  => ready_in(i) ,
            ready_out => ready_out(i),
            valid_out => valid_out(i),
            result    => data_out(i) ,
            modulus   => key_n       ,
            clk       => clk         ,
            reset_n   => reset_n
        );
    end generate exp_cores;
    
	-- CONTROLLER STATE MACHINE
    CombProc : process (curr_state, msgin_valid, ready_in, core_number_in_next, core_number_out_next, 
                        valid_out, msgout_ready, data_out, core_number_in_r, core_number_out_r, 
                        core_in_use_r, msgin_last, core_processes_last_r ) 
        variable v_core_in_use  : std_logic_vector(C_NUMBER_OF_CORES-1 downto 0);
        variable v_valid_in     : std_logic_vector(C_NUMBER_OF_CORES-1 downto 0);
        variable v_ready_out    : std_logic_vector(C_NUMBER_OF_CORES-1 downto 0);
        variable v_core_processes_last : std_logic_vector(C_NUMBER_OF_CORES-1 downto 0);
    begin
        case (curr_state) is   
        when IDLE =>
            core_number_in_next  <= 0;
            core_number_out_next <= 0;
            msgin_ready     <= '0';
            msgout_last     <= '0';
            msgout_valid    <= '0';
            core_in_use     <= (others => '0');
            core_processes_last <= (others => '0');
            msgout_data     <= (others => '0');
            valid_in        <= (others => '0');
            ready_out       <= (others => '0');
            if msgin_valid = '1' then
                next_state  <= ENCODE;
            else
                next_state  <= IDLE;
            end if;
            
        when ENCODE =>
            msgin_ready     <= ready_in(core_number_in_r);
            v_valid_in      := (others => '0');
            v_ready_out     := (others => '0');
            v_core_in_use   := core_in_use_r;
            v_core_processes_last := core_processes_last_r;
            
            v_valid_in(core_number_in_r)    := msgin_valid;
            v_ready_out(core_number_out_r)  := msgout_ready;
            msgout_valid    <= valid_out(core_number_out_r);
            msgout_data     <= data_out(core_number_out_r);
            
            -- Mark selected core as busy and select next core
            if msgin_valid = '1' 
            and ready_in(core_number_in_r) = '1' 
            and core_in_use_r(core_number_in_r) = '0' then
                v_core_in_use(core_number_in_r) := '1';
                if core_number_in_r < C_NUMBER_OF_CORES-1  then
                    core_number_in_next <= core_number_in_r + 1;
                else
                    core_number_in_next <= 0;
                end if;
                
                -- Mark core with last message
                if msgin_last = '1' then
                    v_core_processes_last(core_number_in_r) := '1';
                else
                    v_core_processes_last(core_number_in_r) := '0';
                end if;
            else -- core is not available -> do nothing!
                core_number_in_next <= core_number_in_r;
            end if;
            
            -- Check if core is ready and free resources
            if valid_out(core_number_out_r) = '1' 
            and msgout_ready = '1'
            and core_in_use_r(core_number_out_r) = '1' then
                v_core_in_use(core_number_out_r) := '0';
                if core_number_out_r < C_NUMBER_OF_CORES-1 then
                    core_number_out_next <= core_number_out_r + 1;
                else
                    core_number_out_next <= 0;
                end if;
            else -- no core is not ready -> do nothing!
                core_number_out_next <= core_number_out_r;
            end if;
            
            -- Check if last message is processed
            if v_core_processes_last(core_number_out_r) = '1'
            and valid_out(core_number_out_r) = '1' then
                msgout_last <= '1';
            else
                msgout_last <= '0';
            end if;
            
            core_in_use <= v_core_in_use;
            core_processes_last <= v_core_processes_last;
            valid_in    <= v_valid_in;
            ready_out   <= v_ready_out;
            next_state <= ENCODE;
        
        when others =>
            core_number_in_next  <= 0;
            core_number_out_next <= 0;
            msgin_ready     <= '0';
            msgout_last     <= '0';
            msgout_valid    <= '0';
            core_in_use     <= (others => '0');
            core_processes_last <= (others => '0');
            msgout_data     <= (others => '0');
            valid_in        <= (others => '0');
            ready_out       <= (others => '0');
            next_state  <= IDLE;
        end case;
        
    end process CombProc;
    
    SyncProc : process (reset_n, clk) begin
        if (reset_n = '0') then
            curr_state <= IDLE;
            core_number_in_r    <= 0;
            core_number_out_r   <= 0;
            core_processes_last_r <= (others => '0');
            core_in_use_r       <= (others => '0');
        elsif (clk'event and clk='1') then
            core_number_in_r    <= core_number_in_next;
            core_number_out_r   <= core_number_out_next;
            core_processes_last_r <= core_processes_last;
            core_in_use_r       <= core_in_use;
            curr_state <= next_state;
        end if;
    end process SyncProc;

    rsa_status     <= (others => '0');

end rtl;
